<script src="{{ asset ('lib/jquery/jquery.js') }}"></script>
<script src="{{ asset ('lib/popper.js/popper.js') }}"></script>
<script src="{{ asset ('lib/bootstrap/bootstrap.js') }}"></script>
<script src="{{ asset ('lib/perfect-scrollbar/js/perfect-scrollbar.jquery.js') }}"></script>
<script src="{{ asset ('lib/moment/moment.js') }}"></script>
<script src="{{ asset ('lib/fullcalendar/fullcalendar.js') }}"></script>

<script src="{{ asset ('lib/d3/d3.js') }}"></script>
<script src="{{ asset ('lib/rickshaw/rickshaw.min.js') }}"></script>
<script src="http://maps.google.com/maps/api/js?key=AIzaSyAEt_DBLTknLexNbTVwbXyq2HSf2UbRBU8"></script>
<script src="{{ asset ('lib/gmaps/gmaps.js') }}"></script>
<script src="{{ asset ('lib/chart.js/Chart.js') }}"></script>
<script src="{{ asset ('lib/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset ('lib/datatables-responsive/dataTables.responsive.js')}}"></script>
<script src="{{ asset ('js/katniss.js') }}"></script>
<script src="{{ asset ('js/ResizeSensor.js') }}"></script>
<script src="{{ asset ('js/dashboard.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
@stack('scripts')
</body>
</html>