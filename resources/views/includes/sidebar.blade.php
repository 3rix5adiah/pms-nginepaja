<!-- ##### SIDEBAR MENU ##### -->
<div class="kt-sideleft">
    <label class="kt-sidebar-label">Navigation</label>
    <ul class="nav kt-sideleft-menu">
        @if ((Auth::user()->status!='marketing'))
        <li class="nav-item">
        <a href="/" class="nav-link active @if(Request::segment(1)=='') {{ 'active' }} @endif">
            <i class="icon ion-ios-home-outline"></i>
            <span>Dashboard</span>
        </a>
        </li><!-- nav-item -->
        <li class="nav-item">
            <a href="{{route ('prodak')}}" class="nav-link @if(Request::segment(1)=='prodak') {{ 'active' }} @endif">
                <i class="fa fa-book" aria-hidden="true"></i>
                <span>Data Vila</span>
            </a>
        </li><!-- nav-item -->
        <li class="nav-item @if(Request::segment(1)=='booking' or Request::segment(1)=='booking') {{ 'active menu-open' }} @endif">
        <a href="#" class="nav-link with-sub">
            <i class="icon ion-folder"></i>
            <span>Booking</span>
        </a>
        <ul class="nav-sub">
            <li class="nav-item"><a href="{{route ('booking')}}" class="nav-link @if(Request::segment(1)=='booking') {{ 'active' }} @endif">Data Booking</a></li>
            <li class="nav-item"><a href="{{route ('history')}}" class="nav-link @if(Request::segment(1)=='history') {{ 'active' }} @endif">History Booking </a></li>
        </ul>
    </li><!-- nav-item -->
    @endif
    <li class="nav-item">
        <a href="{{route ('calender')}}" class="nav-link @if(Request::segment(1)=='calender') {{ 'active' }} @endif">
            <i class="fa fa-calendar" aria-hidden="true"></i>
            <span>Kalender Booking</span>
        </a>
    </li><!-- nav-item -->
    </ul>
    </div><!-- kt-sideleft -->
