@extends('master')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset ('lib/datatables/jquery.dataTables.css') }}">
    <!-- ##### MAIN PANEL ##### -->
    <div class="kt-mainpanel">
      <div class="kt-pagetitle">
        <h5>History Booking</h5>
      </div><!-- kt-pagetitle -->
      <div class="kt-pagebody">
        <div class="card pd-20 pd-sm-40">
          <div class="table-wrapper">
            @if ($message = Session::get('success'))
            <div class="alert alert-info alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong>{{ $message }}</strong>
            </div>
            @endif
            <table id="table" class="table display responsive nowrap" data-tables="true">
                <thead>
                    <tr>
                        <th>No</th>
                        <th class="wd-15p">Marketing</th>
                        <th class="wd-20p">Tamu</th>
                        <th class="wd-15p">Vila</th>
                        <th class="wd-10p">Cekin</th>
                        <th class="wd-10p">Cekout</th>
                        <th class="wd-10p">Harga Sewa</th>
                        <th class="wd-10p">Status</th>
                        {{-- <th class="wd-25p"></th> --}}
                    </tr>
                  </thead>
              <body></body>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->
          <!-- BEGIN MODAL HAPUS -->
    <script src="{{ asset ('lib/jquery/jquery.js') }}"></script>
    <script src="{{ asset ('lib/highlightjs/highlight.pack.js') }}"></script>
<script>
  $(document).ready(function () {  
    
    var table = $("#table").DataTable({
    serverSide: true,
    processing: true,
    ajax: "{{ route('listHistory')}}",
columns:[
    {
      orderable: false, 
      searchable: false,
      render: function (data, type, row, meta) {
        return meta.row + meta.settings._iDisplayStart + 1;
      }
    },
    {data: 'marketing', class: "text-center"},
    {data: 'nama_tamu', class: "text-center"},
    {data: 'vila', class: "text-center"},
    {data: 'tanggal_cekin', class: "text-center"},
    {data: 'tanggal_cekout', class: "text-center"},
    {data: 'harga', class: "text-center"},
    {data: 'status', class: "text-center"},
    {data: 'created_at', class: "text-center", visible:false},
    
],
lengthMenu:[[5,15,30], [5,15,30]],
order: [ [5, 'desc'] ],

    });
 });
</script>
@endsection