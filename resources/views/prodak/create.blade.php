<!DOCTYPE html>
@extends('master')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@section('content')
    <!-- ##### MAIN PANEL ##### -->
    <div class="kt-mainpanel">
      <div class="kt-pagetitle">
        <h5>Form Data Vila</h5>
      </div><!-- kt-pagetitle -->
<form role="form" method="POST"action = "{{route ('saveProduct')}}">
    @csrf
      <div class="kt-pagebody">
          <div class="form-layout">
            <div class="row mg-b-25">
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Nama Vila: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="vila" placeholder="Nama Vila" required>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Jumlah Kamar: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="kamar" placeholder="Jumlah kamar" required>
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Kapasitas Vila: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="kapasitas"  placeholder="Kapasitas Vila" required>
                </div>
              </div><!-- col-4 -->
            </div><!-- row -->

            <div class="form-layout-footer">
              <button class="btn btn-default mg-r-5" type="submit">Submit Form</button>
            </div><!-- form-layout-footer -->
          </div><!-- form-layout -->
        </div><!-- card -->
      </div><!-- kt-pagebody -->
    </div><!-- kt-mainpanel -->
</form>
    <script src="{{ asset ('lib/jquery/jquery.js') }}"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>

    </script>
@endsection