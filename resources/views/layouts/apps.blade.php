<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{  'Portal Nginepaja.co.id' }}</title>

    <meta property="og:image" content="http://themepixels.me/katniss/img/katniss-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/katniss/img/katniss-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">


    <title>Katniss Responsive Bootstrap 4 Admin Template</title>
    <!-- Katniss CSS -->
    <link rel="stylesheet" href="../css/katniss.css">
  </head>

  <body>

    <div class="signpanel-wrapper">
      <div class="signbox">
        <div class="signbox-header">
          <h4>katniss</h4>
          <p class="mg-b-0">Responsive Bootstrap 4 Admin Template</p>
        </div><!-- signbox-header -->
        <div class="signbox-body">
          <div class="form-group">
            <label class="form-control-label">Login:</label>
            <input type="email" name="email" placeholder="Enter your email" class="form-control">
          </div><!-- form-group -->
          <div class="form-group">
            <label class="form-control-label">Password:</label>
            <input type="password" name="password" placeholder="Enter your password" class="form-control">
          </div><!-- form-group -->
          <div class="form-group">
            <a href="">Forgot password?</a>
          </div><!-- form-group -->
          <button class="btn btn-dark btn-block">Sign In</button>
          <div class="tx-center bd pd-10 mg-t-40">Not yet a member? <a href="page-signup.html">Create an account</a></div>
        </div><!-- signbox-body -->
      </div><!-- signbox -->
    </div><!-- signpanel-wrapper -->

    <script src="../lib/jquery/jquery.js"></script>
    <script src="../lib/popper.js/popper.js"></script>
    <script src="../lib/bootstrap/bootstrap.js"></script>
  </body>
</html>
