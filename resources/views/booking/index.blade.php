@extends('master')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset ('lib/datatables/jquery.dataTables.css') }}">
    <!-- ##### MAIN PANEL ##### -->
    <div class="kt-mainpanel">
      <div class="kt-pagetitle">
        <h5>Data Bookig</h5>
      </div><!-- kt-pagetitle -->
      <div class="kt-pagebody">
        <div class="card pd-20 pd-sm-40">
          <div class="col-sm-6 col-md-2">
            <h3 class="card-title"><a href="{{route('formBooking')}}" class="btn btn-primary btn-block mg-b-10"><i class="fa fa-plus"> Buat Data</a></h3></i>
          </div>
          <div class="table-wrapper">
            @if ($message = Session::get('success'))
            <div class="alert alert-info alert-block">
                <button type="button" class="close" data-dismiss="alert">×</button> 
                <strong>{{ $message }}</strong>
            </div>
            @endif
            <table id="table" class="table display responsive nowrap" data-tables="true">
                <thead>
                    <tr>
                        <th>No</th>
                        <th class="wd-15p">Marketing</th>
                        <th class="wd-20p">Tamu</th>
                        <th class="wd-15p">Vila</th>
                        <th class="wd-10p">Cekin</th>
                        <th class="wd-10p">Cekout</th>
                        <th class="wd-10p">Harga Sewa</th>
                        <th class="wd-10p">Status</th>
                        <th class="wd-25p"></th>
                        <th class="wd-25p"></th>
                        {{-- <th class="wd-25p"></th> --}}
                    </tr>
                  </thead>
              <body></body>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->
          <!-- BEGIN MODAL HAPUS -->
  <div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Confirmation</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="confirm"></div>
                <input type="hidden" name="" id="id">
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" id="deleteBtn">Ok</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
  
    <script src="{{ asset ('lib/jquery/jquery.js') }}"></script>
    <script src="{{ asset ('lib/highlightjs/highlight.pack.js') }}"></script>
<script>
  $(document).ready(function () {  
    
    var table = $("#table").DataTable({
    serverSide: true,
    processing: true,
    // scrollX: true,
ajax: "{{ route('listBooking')}}",
columns:[
    {
      orderable: false, 
      searchable: false,
      render: function (data, type, row, meta) {
        return meta.row + meta.settings._iDisplayStart + 1;
      }
    },
    {data: 'marketing', class: "text-center"},
    {data: 'nama_tamu', class: "text-center"},
    {data: 'vila', class: "text-center"},
    {data: 'tanggal_cekin', class: "text-center"},
    {data: 'tanggal_cekout', class: "text-center"},
    {data: 'harga', class: "text-center"},
    {data: 'status', class: "text-center"},
    {data: 'action', class: "text-center", orderable: false, searchable: false},
    {data: 'created_at', class: "text-center", visible:false},
    
],
lengthMenu:[[5,15,30], [5,15,30]],
order: [ [5, 'desc'] ],

});

$('[data-tables=true]').on('click', '.destroy', function() {  
    
    $("#modal-default").modal("show");
  			var id = $(this).data('id');
  			$("#id").val(id);
  			var name = $(this).data('nama');
  			$("#confirm").html('Apakah anda yakin untuk menghapus data <b>'+name+'</b> ?' );
    });
        $('#deleteBtn').click(function () {
            var id = $("#id").val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                url: "{{ url('booking/delete')}}",
                type: 'post',
                data: {
                    'id': id,
                },success: function (response) {
                    
                    $("#modal-default").modal("hide");
                    table.ajax.reload( null,false );
                }
                
            });
        });
    });
</script>
@endsection