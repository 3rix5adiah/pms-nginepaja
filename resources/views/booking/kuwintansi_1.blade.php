<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Katniss">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/katniss/img/katniss-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/katniss">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/katniss/img/katniss-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/katniss/img/katniss-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">


    <title>Katniss Responsive Bootstrap 4 Admin Template</title>

    <!-- vendor css -->
    <link href="{{ asset ('lib/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset ('lib/Ionicons/css/ionicons.css') }}" rel="stylesheet">
    <link href="{{ asset ('lib/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet">
    <link href="{{ asset ('lib/rickshaw/rickshaw.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <!-- Katniss CSS -->
    <link rel="stylesheet" href="{{ asset ('css/katniss.css') }}">
    <style>
body{
    margin-top:20px;
    color: #484b51;
}
.text-secondary-d1 {
    color: #728299!important;
}
.page-header {
    margin: 0 0 1rem;
    padding-bottom: 1rem;
    padding-top: .5rem;
    border-bottom: 1px dotted #e2e2e2;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-pack: justify;
    justify-content: space-between;
    -ms-flex-align: center;
    align-items: center;
}
.page-title {
    padding: 0;
    margin: 0;
    font-size: 1.75rem;
    font-weight: 300;
}
.brc-default-l1 {
    border-color: #dce9f0!important;
}

.ml-n1, .mx-n1 {
    margin-left: -.25rem!important;
}
.mr-n1, .mx-n1 {
    margin-right: -.25rem!important;
}
.mb-4, .my-4 {
    margin-bottom: 1.5rem!important;
}

hr {
    margin-top: 1rem;
    margin-bottom: 1rem;
    border: 0;
    border-top: 1px solid rgba(0,0,0,.1);
}

.text-grey-m2 {
    color: #888a8d!important;
}

.text-success-m2 {
    color: #86bd68!important;
}

.font-bolder, .text-600 {
    font-weight: 600!important;
}

.text-110 {
    font-size: 110%!important;
}
.text-blue {
    color: #478fcc!important;
}
.pb-25, .py-25 {
    padding-bottom: .75rem!important;
}

.pt-25, .py-25 {
    padding-top: .75rem!important;
}
.bgc-default-tp1 {
    background-color: rgba(121,169,197,.92)!important;
}
.bgc-default-l4, .bgc-h-default-l4:hover {
    background-color: #f3f8fa!important;
}
.page-header .page-tools {
    -ms-flex-item-align: end;
    align-self: flex-end;
}

.btn-light {
    color: #757984;
    background-color: #f5f6f9;
    border-color: #dddfe4;
}
.w-2 {
    width: 1rem;
}

.text-120 {
    font-size: 120%!important;
}
.text-primary-m1 {
    color: #4087d4!important;
}

.text-danger-m1 {
    color: #dd4949!important;
}
.text-blue-m2 {
    color: #68a3d5!important;
}
.text-150 {
    font-size: 250%!important;
}
.text-60 {
    font-size: 60%!important;
}
.text-grey-m1 {
    color: #7b7d81!important;
}
.align-bottom {
    vertical-align: bottom!important;
}
table {
  border-collapse: collapse;
  border-spacing: 0;
  width: 100%;
  border: 1px solid #ddd;
}

th, td {
  text-align: left;
  padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2
}
    </style>
<body>
<div class="page-content container">
    <div class="page-header text-blue-d2">
        <h1 class="page-title text-secondary-d1">
            kwitansi
            {{-- <small class="page-info">
                <i class="fa fa-angle-double-right text-80"></i>
                ID: #111-222
            </small> --}}
        </h1>
    </div>

    <div class="container px-0">
        <div class="row mt-4">
            <div class="col-12 col-lg-12">
                <div class="row">
                    <div class="col-12">
                        <div class="text-center text-150">
                            <i class="fa fa-book fa-2x text-success-m2 mr-1"></i>
                            <span class="text-default-d3">{{ $data->vila }}</span>
                        </div>
                    </div>
                </div>
                <!-- .row -->

                <hr class="row brc-default-l1 mx-n1 mb-4" />

                <div class="row">
                    <div class="col-sm-6">
                        <div>
                            <span class="text-sm text-grey-m2 align-middle">Terima dari: </span>
                            <span class="text-600 text-110 text-blue align-middle">{{$data->nama_tamu}}</span>
                        </div>
                        <div class="text-grey-m2">
                            <div class="my-1"><i class="fa fa-phone fa-flip-horizontal text-secondary"></i> <b class="text-600">Telpon : {{$data->telpon}}</b></div>
                        </div>
                        <div class="my-2"><i class="fa fa-circle text-blue-m2 text-xs mr-1"></i> <span class="text-600 text-90">Status:</span> <span class="badge badge-warning badge-pill px-25">{{$data->status}}</span></div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th class="center">#</th>
            <th>CekIn</th>
            <th>CekOut</th>
            <th>Total</th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td class="center">1</td>

            <td>
               {{$data->tanggal_cekin}}
            </td>
            <td class="hidden-xs">
                {{$data->tanggal_cekout}}
            </td>
            <td>{{ $Action->numberFormat($data->harga)}}</td>
    </tbody>
</table>
<hr/>
<div class="col-sm-6">
    <div class="text-center text-150">
<h2> Informasi Tambahan :</h2>
    </div>
<div class="my-2"><i class="fa fa-circle text-black-m2 text-xs mr-1"></i> <span class="text-600 text-90">Batas akhir Reschedule(Perubahan jadwal) H -7 dari (Tanggal check in).</span></div>
<div class="my-2"><i class="fa fa-circle text-black-m2 text-xs mr-1"></i> <span class="text-600 text-90">Reschedule(Perubahan jadwal) hanya berlaku di vila yang sama tidak bisa ganti vila.</span></div>
<div class="my-2"><i class="fa fa-circle text-black-m2 text-xs mr-1"></i> <span class="text-600 text-90">Tanggal ganti Reschedule di tentukan oleh pihak Vila.</span></div>
<div class="my-2"><i class="fa fa-circle text-black-m2 text-xs mr-1"></i> <span class="text-600 text-90">Reschedule akan dikenakan biaya tambahan.</span></div>
<div class="my-2"><i class="fa fa-circle text-black-m2 text-xs mr-1"></i> <span class="text-600 text-90">Batas akhir pelunasan H -7 dari (Tanggal check in).</span></div>
<div class="my-2"><i class="fa fa-circle text-black-m2 text-xs mr-1"></i> <span class="text-600 text-90">Jika cancel DP tidak bisa dikembalika.</span></div>
</div>
</span></div>
</body>
</html>
