@extends('master')
@section('content')
    <!-- ##### MAIN PANEL ##### -->
    <div class="kt-mainpanel">
      <div class="kt-pagetitle">
        <h5>Data Tables</h5>
      </div><!-- kt-pagetitle -->

      <div class="kt-pagebody">

        <div class="card pd-20 pd-sm-40">
          <h6 class="card-body-title">Basic Responsive DataTable</h6>
          <p class="mg-b-20 mg-sm-b-30">Searching, ordering and paging goodness will be immediately added to the table, as shown in this example.</p>

          <div class="table-wrapper">
            <table id="table" class="table display responsive nowrap" data-table="true">
                <thead>
                    <tr>
                      <td><b>No</b></td>
                      <td><b>Kode Booking</b></td>
                      <td><b>Nama Tamu</b></td>
                      <td><b>Nama Vila</b></td>
                      <td><b>Tanggal Cekin</b></td>
                      <td><b></b></td>
                      <td><b></b></td>
                    </tr>
                  </thead>
              <body></body>
            </table>
          </div><!-- table-wrapper -->
        </div><!-- card -->

      </div><!-- kt-pagebody -->
   
    </div><!-- kt-mainpanel -->
  
    <script src="{{ asset ('lib/jquery/jquery.js') }}"></script>
    <script src="{{ asset ('lib/highlightjs/highlight.pack.js') }}"></script>
<script>
  $(document).ready(function () {
    var table = $("#table").DataTable({
    serverSide: true,
    processing: true,
ajax: "{{ route('listBooking')}}",
columns:[
    {
      orderable: false, 
      searchable: false,
      render: function (data, type, row, meta) {
        return meta.row + meta.settings._iDisplayStart + 1;
      }
    },
    {data: 'kode_booking', class: "text-center"},
    {data: 'nama_tamu', class: "text-center"},
    {data: 'Vila', class: "text-center"},
    {data: 'tanggal_cekin', class: "text-center"},
    {data: 'action', class: "text-center", orderable: false, searchable: false},
    {data: 'created_at', class: "text-center", visible:false},
    
],
order: [ [4, 'desc'] ],

});
$('[data-tables=true]').on('click', '.destroy', function() {    
    $("#modal-default").modal("show");
  			var id = $(this).data('id');
  			$("#id").val(id);
  			var name = $(this).data('nama');
  			$("#confirm").html('Apakah anda yakin untuk menghapus data ku <b>'+name+'</b> ?' );
    });
        $('#deleteBtn').click(function () {
            var id = $("#id").val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                url: "",
                type: 'post',
                data: {
                    'id': id,
                },success: function (response) {
                    
                    $("#modal-default").modal("hide");
                    table.ajax.reload( null,false );
                }
                
            });
        });
    });
</script>
@endsection