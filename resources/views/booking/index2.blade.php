@extends('master')
<link rel="stylesheet" type="text/css" href="{{ asset ('lib/datatables/jquery.dataTables.css') }}">
@section('content')
    <!-- ##### MAIN PANEL ##### -->
    <div class="kt-mainpanel">
      <div class="kt-pagetitle">
        <h5>Data Tables</h5>
      </div><!-- kt-pagetitle -->
    <div class="kt-pagebody">

<div class="card pd-20 pd-sm-40">
        <h6 class="card-body-title">Basic Responsive DataTable</h6>
        <p class="mg-b-20 mg-sm-b-30">Searching, ordering and paging goodness will be immediately added to the table, as shown in this example.</p>
    <div class="table-wrapper">
        <table id="datatable1" class="table display responsive nowrap" data-tables="true">
            <thead>
            <tr>
                <th class="wd-15p">First name</th>
                <th class="wd-15p">Last name</th>
                <th class="wd-20p">Position</th>
                <th class="wd-15p">Start date</th>
                <th class="wd-10p">Salary</th>
                <th class="wd-25p">E-mail</th>
            </tr>
            </thead>
            <tbody> </tbody>
        </table>
    </div><!-- table-wrapper -->
</div><!-- card -->
@endsection
@push('scripts')
<script type="text/javascrip"></script>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="{{ asset('lib/datatables/jquery.dataTables.js')}}"></script>
<script src="{{ asset('lib/datatables-responsive/dataTables.responsive.js')}}"></script>

<script>
  $(document).ready(function () {
    var table = $("#table").DataTable({
    serverSide: true,
    processing: true,
ajax: "{{route ('listBooking')}}",
columns:[
    {
      orderable: false, 
      searchable: false,
      render: function (data, type, row, meta) {
        return meta.row + meta.settings._iDisplayStart + 1;
      }
    },
    {data: 'kode_booking', class: "text-center"},
    {data: 'nama_tamu', class: "text-center"},
    {data: 'Vila', class: "text-center"},
    {data: 'action', class: "text-center", orderable: false, searchable: false},
    { data: 'created_at', class: "text-center", visible:false},
],
order: [ [1, 'desc'] ],

});
$('[data-tables=true]').on('click', '.destroy', function() {    
    $("#modal-default").modal("show");
  			var id = $(this).data('id');
  			$("#id").val(id);
  			var name = $(this).data('nama');
  			$("#confirm").html('Apakah anda yakin untuk menghapus data ku <b>'+name+'</b> ?' );
    });
        $('#deleteBtn').click(function () {
            var id = $("#id").val();
            $.ajax({
                headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
                url: "{{ url('data-ku/delete') }}",
                type: 'post',
                data: {
                    'id': id,
                },success: function (response) {
                    
                    $("#modal-default").modal("hide");
                    table.ajax.reload( null,false );
                }
                
            });
        });
    });
</script>

    