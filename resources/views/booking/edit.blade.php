<!DOCTYPE html>
@extends('master')
@section('content')
    <!-- ##### MAIN PANEL ##### -->
    <div class="kt-mainpanel">
      <div class="kt-pagetitle">
        <h5>Form Edit Booking</h5>
      </div><!-- kt-pagetitle -->
<form role="form" method="POST"action = "{{route ('updateBooking')}}">
    @csrf
      <div class="kt-pagebody">
          <div class="form-layout">
            <div class="row mg-b-25">
              <div class="col-lg-4">
                <div class="form-group">
                  <input type="hidden" class="form-control" name="id" value="{{$data->id}}">
                  <label class="form-control-label">Marketing: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="marketing" value="{{$data->marketing}}" placeholder="Nama Marketing">
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Nama Tamu: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="nama_tamu" value="{{$data->nama_tamu}}" placeholder="Nama Tamu">
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Telpon Tamu: <span class="tx-danger">*</span></label>
                  <input class="form-control" inputmode="numeric" name="telpon" value="{{$data->telpon}}" oninput="this.value = this.value.replace(/\D+/g, '')"  />
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Nama Vila: <span class="tx-danger">*</span></label>
                  <select class="form-control select2bs4" style="width: 100%;" name="idProduct">
                    <option value="" id="KategoriOp"> --- Pilih Vila  ---</option>
                     @foreach($product as $row)
                     <option value="{{$row->id}}"  @if($data->idProduct == $row->id) {{ 'selected'}} @endif>{{ $row->vila }}</option>
                          <!-- <option value="{{$row->id}}">{{ $row->vila }}</option> -->
                     {{-- <option value="{{$row->vila}}"  @if($data->id == $row->id) {{ 'selected'}} @endif>{{ $row->vila}}</option> --}}
                     @endforeach
                    </select>
                </div>
              </div><!-- col-4 -->
              {{-- <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Nama Vila: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="vila" value="{{$data->vila}}"  placeholder="Nama Vila">
                </div>
              </div><!-- col-4 --> --}}
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Tanggal Cekin: <span class="tx-danger">*</span></label>
                  <input class="date form-control" type="datetime-local" id="startDate" value="{{$data->tanggal_cekin}}" name="tanggal_cekin">
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Tanggal Cekout: <span class="tx-danger">*</span></label>
                  <input class="date form-control" type="datetime-local" id="startDate"  value="{{$data->tanggal_cekout}}" name="tanggal_cekout">
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group">
                  <label class="form-control-label">Harga Sewa: <span class="tx-danger">*</span></label>
                  <input class="form-control" type="text" name="harga" value="{{$data->harga}}" placeholder="Harga Sewa">
                </div>
              </div><!-- col-4 -->
              <div class="col-lg-4">
                <div class="form-group mg-b-10-force">
                  <label class="form-control-label">Status Booking: <span class="tx-danger">*</span></label>
                  <select class="form-control select2" data-placeholder="Choose country" name="status">
                    <option label="Pilih"></option>
                    <option value="DP" {{ ( $data->status == 'DP') ? 'selected' : '' }}>DP</option>
                    <option value="Lunas" {{ ( $data->status == 'Lunas') ? 'selected' : '' }}>Lunas</option>
                    <option value="Cancel" {{ ( $data->status == 'Cancel') ? 'selected' : '' }}>Cancel</option>
                  </select>
                </div>
              </div><!-- col-4 -->
            </div><!-- row -->

            <div class="form-layout-footer">
              <button class="btn btn-default mg-r-5" type="submit">Submit Form</button>
              {{-- <button class="btn btn-secondary">Cancel</button> --}}
            </div><!-- form-layout-footer -->
          </div><!-- form-layout -->
        </div><!-- card -->
      </div><!-- kt-pagebody -->
    </div><!-- kt-mainpanel -->
</form>
    <script src="{{ asset ('lib/jquery/jquery.js') }}"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script>
      $('#startDate').datetimepicker({
		  format:	'Y-m-d H:i'
	    });
      $('#endDate').datetimepicker({
		  format:	'Y-m-d H:i'
	    });
    //     config = {
    //     enableTime: true,
    //     dateFormat: 'Y-m-d H:i',
    //     autoclose: true
    // }
    //     flatpickr("input[type=datetime-local]", config);
    // </script>
@endsection