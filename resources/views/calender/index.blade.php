<!DOCTYPE html>
@extends('master')
@section('title_left')
Kalender
@endsection 
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset ('lib/fullcalendar/fullcalendar.css') }}">

    <!-- ##### MAIN PANEL ##### -->
    <div class="kt-mainpanel">
      <div class="kt-pagetitle">
        <h5>Calendar Booking</h5>
      </div><!-- kt-pagetitle -->

      <div class="kt-pagebody">
        <div id="calendar"></div>
      </div><!-- kt-pagebody -->
      <div class="card-header bg-gray-100 pd-y-15 pd-x-20">
        <h6 class="card-title tx-uppercase tx-12 mg-b-0">Keterangan :</h6>
        <div>
        <span class="square-8 bg-success mg-r-5 rounded-circle"></span> Booking
        </div>
        <td class="tx-12">
        <span class="square-8 bg-red mg-r-5 rounded-circle"></span> Cancel
        </td>
      </div><!-- card-header -->

    </div><!-- kt-mainpanel -->

    <script src="{{ asset ('lib/jquery/jquery.js') }}"></script>
      <script>
    $(document).ready(function () {  
        var booking = @json($events);
        console.log(booking)
        $('#calendar').fullCalendar({
          header: {
            left:   'prev',
            center: 'title',
            right:  'today next'
          },
          events:booking,
          eventTextColor: 'white',
          displayEventTime: false
        })
      });
    </script>
@endsection
