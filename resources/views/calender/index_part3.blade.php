<!DOCTYPE html>
@extends('master')
@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset ('lib/fullcalendar/fullcalendar.css') }}">

    <!-- ##### MAIN PANEL ##### -->
    <div class="kt-mainpanel">
      <div class="kt-pagetitle">
        <h5>Calendar Booking</h5>
      </div><!-- kt-pagetitle -->

      <div class="kt-pagebody">

        <div id="calendar"></div>
      </div><!-- kt-pagebody -->
    </div><!-- kt-mainpanel -->
    <div class="modal fade" id="modal-default">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <h4 class="modal-title">Confirmation</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <div id="confirm"></div>
                  <input type="hidden" name="" id="id">
              </div>
              <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary" id="deleteBtn">Ok</button>
              </div>
          </div>
          <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
  </div>
    <script src="{{ asset ('lib/jquery/jquery.js') }}"></script>
    <script>
      $(document).ready(function () {
              // page is now ready, initialize the calendar...
             
              bookings={!! json_encode($events) !!};
              console.log(bookings)
              $('#calendar').fullCalendar({
                  // put your options and callbacks here
                  header: {
                  left:   'prev',
                  center: 'title',
                  right:  'today next',
                  eventLimit: true,
                },
                  events: bookings
  
              });
          });
  </script>
   {{-- <script>
    $(document).ready(function () {  

        var booking = @json($events);
        console.log(booking)
        $('#calendar').fullCalendar({
          header: {
            left:   'prev',
            center: 'title',
            right:  'today next'
          },
          events:booking,
          selectable: true,
          selectHelper: true,
          select: function(start,end,allDays){
            $("#modal-default").modal("show");
          }
        })
      });
    </script> --}}
@endsection
