<!doctype html>
<html lang="en">
<head>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.js"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>


    <style>
        /* ... */
    </style>
</head>
<body>
    {!! $calendar->calendar() !!}
    {!! $calendar->script() !!}
</body>
</html>
<script>
    $(document).ready(function(){
        $('#calendar-vggDK65l').fullCalendar({
            "header":{
            "left":"prev,next today",
            "center":"title",
            "right":"month,agendaWeek,agendaDay"},
            "eventLimit":true,
            "events":[{
                "id":6,
                "title":"Vila Bu onah",
                "allDay":false,
                "start":"2023-09-03T17:46:56+00:00",
                "end":"2023-09-05T22:46:56+00:00"},
                {
                    "id":7,
                "title":"Vila malak 4",
                "allDay":false,
                "start":"2023-09-09T22:52:13+00:00",
                "end":"2023-09-10T22:52:13+00:00"}]});
    });
</script>
