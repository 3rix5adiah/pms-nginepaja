<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware(['auth'])->group(function () {
Route::get('/', 'DashboardController@index')->name('dashboard');

Route::group(['prefix' => 'booking'], function () {
    Route::get('/', 'BookingController@index')->name('booking');
    Route::get('/list','BookingController@getList')->name('listBooking');
    Route::get('/create', 'BookingController@form')->name('formBooking');
    Route::post('/', 'BookingController@create')->name('saveBooking');
    Route::get('/{id}', 'BookingController@edit');
    Route::post('/update', 'BookingController@update')->name('updateBooking');;
    Route::post('/delete', 'BookingController@destroy')->name('deleteBooking');
    Route::get('/download/{id}', 'BookingController@download')->name('download');
});
Route::group(['prefix' => 'calender'], function () {
    Route::get('/', 'CalenderController@index')->name('calender');
});
Route::group(['prefix' => 'history-booking'], function () {
    Route::get('/', 'HistoryController@index')->name('history');
    Route::get('/list','HistoryController@getListHistory')->name('listHistory');
});
Route::group(['prefix' => 'prodak'], function () {
    Route::get('/', 'ProductController@index')->name('prodak');
    Route::get('/list','ProductController@getListProdak')->name('listProduct');
    Route::get('/create', 'ProductController@form')->name('formProduct');
    Route::post('/', 'ProductController@create')->name('saveProduct');
    Route::get('/{id}', 'ProductController@edit');
    Route::post('/update', 'ProductController@update')->name('updateProduct');
    Route::post('/delete', 'ProductController@destroy')->name('deleteProduct');
});

});

Auth::routes();
