<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class Action extends Model
{
	public function __construct()
    {
        // $this->Date = new Date;
    }

	function CheckError($check,$messageError)
	{
      if(!$check){
        throw new Exception($messageError); 
      }
      try {  
		  
		   //empty

		 }  

	  catch(Exception $e) {  

	    return 'messageError: ' .$e->getMessage();  

      }  
	} 

    function GetField($table,$field_name,$where,$get_field)
	{
      $data=DB::table($table)
		  ->where($field_name,$where)
		  ->select($get_field)
		  ->first();
	   if($data){

		  return $data->$get_field;
	   }	
	   return "";  
	}

	function GetFirst($table,$field_name,$where)
	{
      $data=DB::table($table)
		  ->where($field_name,$where)
		  ->first();
	   if($data){

		  return $data;
	   }	
	   return "";  
	}

	function GetTab($table,$field_name,$where)
	{
      $data=DB::table($table)
		  ->where($field_name,$where)
		  ->get();
	   if($data){

		  return $data;
	   }	
	   return "";  
	}

	function GetTableDis($table,$field_name,$where, $rw='')
	{
		$data=DB::table($table)
		->select('rw')
		->distinct()
		->where($field_name,$where)
		->get();

	if($rw){
		$data=DB::table($table)
		->select('rw')
		->distinct()
		->where($field_name,$where)
		->where('rw',$rw)
		->get();
	}	
	 if($data){

		return $data;
	 }	
	 return "";  
	}
     
    function GetTab2($table,$field_name,$where,$field_name1,$where1)
	{
      $data=DB::table($table)
		  ->where($field_name,$where)
		   ->where($field_name1,$where1)
		  ->get();
	   if($data){

		  return $data;
	   }	
	   return "";  
	}
	  
	function GetField2($table,$field_name1,$where1,$field_name2,$where2,$get_field)
	{
      $data=DB::table($table)
		  ->where($field_name1,$where1)
		  ->where($field_name2,$where2)
		  ->select($get_field)
		  ->first();
	   if($data){

		  return $data->$get_field;
	   }	
	   return "";  
	}

	function GetCount($table,$where,$valWehere){

		$data = DB::table($table)->where($where,$valWehere)->count();
		return $data;
	}

	function getSum($table,$where,$valWehere, $field){

		$data = DB::table($table)->where($where,$valWehere)->sum($field);
		return $data;
	}

	function GetDiscount($table,$where,$valWehere){

		$data = DB::table($table)->where($where,$valWehere)->count();
		return $data;
	}

	function GetCount2($table,$where,$valWehere,$where1,$valWehere1){

		$data = DB::table($table)
		->where($where,$valWehere)
		->where($where1,$valWehere1)
		->count();
		return $data;
	}

	function sumTotal($valWehere, $valWehere1, $periode){

		if($periode){
			$periode = $this->Date->monthYear($periode);
		}else{
			$periode = date("F-Y", strtotime("-1 months"));
		}
		
		$data = DB::table('pembayarans as p')
		->where('p.zona_id',$valWehere)
		->where('p.rw',$valWehere1)
		->where('p.periode',$periode)
		->get();
		$total = 0;
		foreach ($data as $item) {
			$total += $item->total_bayar;
			
		}
		return $total;
	}

	function getNotPaid($area, $rw, $periode){

		if($periode){
			$periode = $this->Date->monthYear($periode);
		}else{
			$periode = date("F-Y", strtotime("-1 months"));
		}
		$param = DB::table('pembayarans')
		->select('pelanggan_id')
		->where('zona_id',$area)
		->where('periode',$periode)
		->where('rw', $rw)
		->get();

		$params = [];
		foreach($param as $row){
		  $params[]=$row->pelanggan_id;
		}

		$pelangganId = DB::table('pelanggans')
		->where('perumahan',$area)
		->where('rw',$rw)
		->where('status',2)
		->where('type',null)
		->whereNotIn('id',$params)->count();
		return $pelangganId;
	}
	

	function getPaid($area, $rw, $periode){

		if($periode){
			$periode = $this->Date->monthYear($periode);
		}else{
			$periode = date("F-Y", strtotime("-1 months"));
		}
	
	   
		$param = DB::table('pembayarans')
		->select('pelanggan_id')
		->where('zona_id',$area)
		->where('periode',$periode)
		->where('rw',$rw)
		->get();
		$params = [];
		foreach($param as $row){
		  
		  $params[]=$row->pelanggan_id;
		}
	
		$pelangganId = DB::table('pelanggans')
		->where('perumahan',$area)
		->where('rw',$rw)
		 ->where('status',2)
		->where('type',null)
		->whereIn('id',$params)->count();
		return $pelangganId;
	}

	function getInvoice($area, $rw, $periode){

		if($periode){
			$periode = $this->Date->monthYear($periode);
		}else{
			$periode = date("F-Y", strtotime("-1 months"));
		}
	
		$pelangganId = DB::table('pelanggans')
		->where('perumahan',$area)
		->where('rw',$rw)
		->where('status',2)
		->where('type',null)
		->count();
		return $pelangganId;
	}
	

	function GetCountDistint($table,$where,$valWehere,$where1,$valWehere1){

		$data = DB::table($table)
		->select('rt')
		->distinct()
		->where($where,$valWehere)
		->where($where1,$valWehere1)
		->get();
		return count($data);
	}

	function GetCount3($table,$where,$valWehere,$where1,$valWehere1,$where2,$valWehere2){
		$data = DB::table($table)
		->where($where,$valWehere)
		->where($where1,$valWehere1)
		->where($where2,$valWehere2)
		->count();
		return $data;
	}


	function NumberFormatNoRp($price)
	{
     return number_format($price,0,',','.').',-';
	}

	function NumberFormat($price)
	{
     return "Rp ".number_format($price,0,',','.').',-';
	}

	function GeneralNumber($number)
	{
        $data = str_replace(array('Rp','.',',','-'),'',$number);
        return $data;
	}
	function FormatNumber($number)
	{
        $data = str_replace('.','',$number);
        return $data;
	}
    
	function TglIndoToMysql($time)
	{
      $value=strtotime("$time");
      return date("Y-m-d H:i", $value);
	}
	function GetMargin($purchase,$sales)
	{
		$output=false;
		
		$margin=(($sales - $purchase) / $purchase) * 100;
		$output=round($margin);
		
		return $output;
		
	}
	function GetMargin2($purchase,$sales)
	{
		$output=false;
		
		// sales - 10%
		// $tax=($sales * 10) / 100;
		// $sales=$sales - $tax;
		$sales_wo_tax=($sales / 1.1);
		
		$margin=(($sales_wo_tax - $purchase) / $purchase) * 100;
		$output=round($margin);
		return $output;
	}	
	function GetNum($table,$field_name,$where)
	{
		
		$query=DB::table($table)->where($field_name,$where)->count();	
		
		return $query;
	}
	function GetPrice($id){
		$query =  DB::table('orders as o')
        ->join('order_details as od', 'od.order_id','=','o.id')
        ->join('products as p','od.product_id','=','p.id')
        ->join('price_product as pp','od.price_product_id','=','pp.id')
		->where('od.order_id', $id)
		->get();
		$total = 0;
        // $totalKebab = 0;
        foreach($query as $row){
           if($row->type == 0) {
            $total += $row->price * $row->qty;
            // if($row->product_id==4){
            //   $totalKebab += $row->price * $row->qty;
            // }
           }
         
           if($row->type == 1) {
            $total += $row->price_sales * $row->qty;
                // if($row->product_id==4){
                // $totalKebab += $row->price_sales * $row->qty;
                // }
            }

		   }
		   
       return $total;
	}
	function GetMaterial($id){
		$query =  DB::table('material_headers as mh')
        ->join('material_orders as mo', 'mo.material_header_id','=','mh.id')
        ->join('material as m','mo.material_id','=','m.id')
		->where('mh.id', $id)
		->get();
		$total = 0;
        // $totalKebab = 0;
        foreach($query as $row){
            $total += $row->price * $row->qty;
		   }
		   
       return $total;
	}
	function GetSumQtyProduct($id){
		$query =  DB::table('order_details')
		->where('price_product_id', $id)
		->get();
		$total = 0;
        // $totalKebab = 0;
        foreach($query as $row){
            $total += $row->qty;
		   }
		   
       return $total;
	}
	function GetNoInvoice($id)
	{
		$output=false;
		
		$length=strlen($id);
		switch($length) {
			case 1 :
			$output='INV-0000'.$id;
			break;
			case 2 :
			$output='INV-000'.$id;
			break;
			case 3 :
			$output='INV-00'.$id;
			break;
			case 4 :
			$output='INV-0'.$id;
			break;
			default:
			$output='INV-'.$id;
		}
		
		return $output;
	}

	function GetNoFaktur($id)
	{
		$output=false;
		
		$length=strlen($id);
		switch($length) {
			case 1 :
			$output='FKT-0000'.$id;
			break;
			case 2 :
			$output='FKT-000'.$id;
			break;
			case 3 :
			$output='FKT-00'.$id;
			break;
			case 4 :
			$output='FKT-0'.$id;
			break;
			default:
			$output='FKT-'.$id;
		}
		
		return $output;
	}

	function penyebut($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
    $temp = "";
    if ($nilai < 12) {
      $temp = " ". $huruf[$nilai];
    } else if ($nilai <20) {
      $temp = $this->penyebut($nilai - 10). " Belas";
    } else if ($nilai < 100) {
      $temp = $this->penyebut($nilai/10)." Puluh". $this->penyebut($nilai % 10);
    } else if ($nilai < 200) {
      $temp = " Seratus" . $this->penyebut($nilai - 100);
    } else if ($nilai < 1000) {
      $temp = $this->penyebut($nilai/100) . " Ratus" . $this->penyebut($nilai % 100);
    } else if ($nilai < 2000) {
      $temp = " Seribu" . $this->penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
      $temp = $this->penyebut($nilai/1000) . " Ribu" . $this->penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
      $temp = $this->penyebut($nilai/1000000) . " Juta" . $this->penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
      $temp = $this->penyebut($nilai/1000000000) . " Milyar" . $this->penyebut(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
      $temp = $this->penyebut($nilai/1000000000000) . " Trilyun" . $this->penyebut(fmod($nilai,1000000000000));
    }     
    return $temp;
  }

  function terbilang($nilai) {
    if($nilai<0) {
      $hasil = "minus ". trim($this->penyebut($nilai));
    } else {
      $hasil = trim($this->penyebut($nilai));
    }         
    return $hasil." Rupiah ";
  }

}
