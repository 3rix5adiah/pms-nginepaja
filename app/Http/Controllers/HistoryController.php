<?php

namespace App\Http\Controllers;
use App\Models\BookingModel;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Action;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request) 
    { 
        $this->Action = new Action;
        
  
    } 
    
    public function index()
    {
        return view('history-booking.index', get_object_vars($this)); 
    }

    public function getListHistory(Request $request){
        // $user = Auth::user()->name;
        // if($user =='admin'){
        //     $booking = DB::table('tbl_booking')
        //     ->select(['id','marketing','nama_tamu','vila','tanggal_cekin','tanggal_cekout','harga','status','created_at'])
        //     ->whereRaw('tanggal_cekout <= curdate()')
        //     ->get();
        // }else{
        //     $booking = DB::table('tbl_booking')
        //     ->select(['id','marketing','nama_tamu','vila','tanggal_cekin','tanggal_cekout','harga','status','created_at'])
        //     ->where('created_by','=',$user)
        //     ->whereRaw('tanggal_cekout <= curdate()')
        //     ->get();
        // }
        $user = Auth::user()->name;
        $booking = DB::table('tbl_booking as p')
        ->join('tbl_product as pe', 'p.idProduct','=','pe.id')   
        
        ->select([
            'p.id',
            'p.marketing',
            'p.nama_tamu',
            'p.tanggal_cekin',
            'p.tanggal_cekout',
            'p.harga',
            'p.status',
            'p.created_at',
            'pe.vila'])
            ->where('p.created_by','=',$user)
            ->whereRaw('p.tanggal_cekout <= curdate()')
            ->get();
        
        $datatables = Datatables::of($booking)->addColumn('action', function ($booking) {
        // return '<a href="booking/download/'.$booking->id.'" target="_blank" data-id="'.$booking->id.'"  class="btn btn-xs btn-info"><i class="fa fa-download"></i></i></a>';

        })

        ->addColumn('status', function($data) {
            if($data->status == 'DP'){
                return '<span class="badge badge-info">DP</span>';
            }
            if($data->status == 'Lunas'){
                return '<span class="badge badge-success">Lunas</span>';
            }
            if($data->status == 'Cancel'){
                return '<span class="badge badge-danger">Cancel</span>';
            }
           
        })
        ->addColumn('harga', function ($data) {
            return $this->Action->NumberFormat($data->harga);
         }) 
        ->escapeColumns([]);

        return $datatables->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
