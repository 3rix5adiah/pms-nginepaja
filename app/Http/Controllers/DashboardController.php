<?php

namespace App\Http\Controllers;
use DB;
use App\Models\BookingModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\DataProdakModel;
use Carbon\Carbon;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user()->name;
        $this->proses = DB::table('tbl_booking')
        ->whereMonth('tanggal_cekin', Carbon::now()->month)
        ->where('status', 'Lunas')
        ->where('created_by', '=',$user)
        ->count();
        // dd($this);
        $this->cancel = DB::table('tbl_booking')
        ->whereMonth('tanggal_cekin', Carbon::now()->month)
        ->where('status', 'cancel')
        ->where('created_by', '=',$user)
        ->count();

        $this->pricemount = DB::table('tbl_booking')
        ->whereMonth('tanggal_cekin', Carbon::now()->month)
        ->where('status', 'Lunas')
        ->where('created_by', '=',$user)
        ->sum('harga');

        $this->priceyear = DB::table('tbl_booking')
        ->whereYear('tanggal_cekin', Carbon::now()->format('Y'))
        ->where('status', 'Lunas')
        ->where('created_by', '=',$user)
        ->sum('harga');
    // price for data cancel
        $this->pricecancelmount = DB::table('tbl_booking')
        ->whereMonth('tanggal_cekin', Carbon::now()->month)
        ->where('status', 'cancel')
        ->where('created_by', '=',$user)
        ->sum('harga');

        $this->pricecancelyear = DB::table('tbl_booking')
        ->whereYear('tanggal_cekin', Carbon::now()->format('Y'))
        ->where('status', 'cancel')
        ->where('created_by', '=',$user)
        ->sum('harga');
        $data = DataProdakModel::paginate(5);
        // $data['rows'] = DataProdakModel::all();
        return view('admin.dashboard', get_object_vars($this),compact('data'));

        // return view('admin.dashboard');
        // $this->proses = DB::table('tbl_booking')
        // ->where('status', 'Lunas')
        // ->count();
        // $this->cancel = DB::table('tbl_booking')
        // ->where('status', 'cancel')
        // ->count();
        // return view('admin.dashboard', get_object_vars($this)); 
        // return view('admin.dashboard');
    }

    public function getListProses(Request $request){
        $user = Auth::user()->name;
            $booking = DB::table('tbl_booking')
            ->select(['id','kode_booking','nama_tamu','vila','tanggal_cekin','tanggal_cekout','status','created_at'])
            ->where('created_by','=',$user)
            ->get();
        
        $datatables = Datatables::of($booking)->addColumn('action', function ($booking) {
        return '<a href="booking/'. $booking->id .'" "  class="btn btn-xs btn-primary"><i class="icon ion-edit" aria-hidden="true"></i></i></a>  <a href="#" data-id="'.$booking->id.'" data-nama="'.$booking->nama_tamu.'"class="btn btn-xs btn-danger destroy"><i class="icon ion-trash-a" ></i></a>';
        })
        ->addColumn('status', function($data) {
            if($data->status == 'DP'){
                return '<span class="badge badge-info">Onproses</span>';
            }
            if($data->status == 'Selesai'){
                return '<span class="badge badge-success">Selesai</span>';
            }
            if($data->status == 'Cancel'){
                return '<span class="badge badge-danger">Cancel</span>';
            }
           
        })
        ->escapeColumns([]);

        return $datatables->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
