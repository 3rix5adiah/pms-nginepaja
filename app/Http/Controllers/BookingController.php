<?php

namespace App\Http\Controllers;
use App\Models\BookingModel;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Action;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use PDF;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(Request $request) 
    { 
        $this->Action = new Action;
        
  
    } 

    public function index()
    {
        return view('booking.index', get_object_vars($this)); 
    }
    public function form()
    {
        $user = Auth::user()->name;
        if($user =='admin'){
            $this->product = DB::table('tbl_product')
            ->orderBy('id', 'asc')->get();
            return view('booking.create', get_object_vars($this));
        }else{
            $this->product = DB::table('tbl_product')
            ->where('created_by','=',$user)
            ->orderBy('id', 'asc')->get();
            return view('booking.create', get_object_vars($this));
        }
   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $booking = new BookingModel;
        $booking->marketing = $request->marketing;
        $booking->nama_tamu = $request->nama_tamu;
        $booking->telpon = $request->telpon;
        $booking->vila = "vila kuu";
        $booking->idProduct = $request->idProduct;
        $booking->tanggal_cekin = $request->tanggal_cekin;
        $booking->tanggal_cekout = $request->tanggal_cekout;
        $booking->status = $request->status;
        $booking->harga = $request->harga;
        $booking->created_by = Auth::user()->name;
        $booking->save();
        return redirect('booking')->withSuccess('Data Berhasil Disimpan ');;
    }

    
    public function getList(Request $request){
        $user = Auth::user()->name;
        $booking = DB::table('tbl_booking as p')
        ->join('tbl_product as pe', 'p.idProduct','=','pe.id')   
        
        ->select([
            'p.id',
            'p.marketing',
            'p.nama_tamu',
            'p.tanggal_cekin',
            'p.tanggal_cekout',
            'p.harga',
            'p.status',
            'p.created_at',
            'pe.vila'])
            ->where('p.created_by','=',$user)
            ->whereRaw('p.tanggal_cekout>= curdate()')
            ->get();
        // $user = Auth::user()->name;
        // if($user =='admin'){
        //     $booking = DB::table('tbl_booking')
        //     ->select(['id','marketing','nama_tamu','vila','tanggal_cekin','tanggal_cekout','harga','status','created_at'])
        //     ->whereRaw('tanggal_cekout>= curdate()')
        //     ->get();
        // }else{
        //     $booking = DB::table('tbl_booking')
        //     ->select(['id','marketing','nama_tamu','vila','tanggal_cekin','tanggal_cekout','harga','status','created_at'])
        //     ->where('created_by','=',$user)
        //     ->whereRaw('tanggal_cekout>= curdate()')
        //     ->get();
        // }
        
        $datatables = Datatables::of($booking)->addColumn('action', function ($booking) {
        return '<a href="booking/'. $booking->id .'" "  class="btn btn-xs btn-primary"><i class="icon ion-edit" aria-hidden="true"></i></i></a>  
        <a href="#" data-id="'.$booking->id.'" data-nama="'.$booking->nama_tamu.'"class="btn btn-xs btn-danger destroy"><i class="icon ion-trash-a" ></i></a>
        <a href="booking/download/'.$booking->id.'" target="_blank" data-id="'.$booking->id.'"  class="btn btn-xs btn-info"><i class="fa fa-download"></i></i></a>';

        })

        ->addColumn('status', function($data) {
            if($data->status == 'DP'){
                return '<span class="badge badge-info">DP</span>';
            }
            if($data->status == 'Lunas'){
                return '<span class="badge badge-success">Lunas</span>';
            }
            if($data->status == 'Cancel'){
                return '<span class="badge badge-danger">Cancel</span>';
            }
           
        })
        ->addColumn('harga', function ($data) {
            return $this->Action->NumberFormat($data->harga);
         }) 
        ->escapeColumns([]);

        return $datatables->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function download($id){
        // $this->data = BookingModel::find($id);
        // $pdf = PDF::loadView('booking.kuwintansi_1', get_object_vars($this));
        $this->data = DB::table('tbl_booking as p')
        ->join('tbl_product as pe', 'p.idProduct','=','pe.id')   
        
        ->select([
            'p.id',
            'p.marketing',
            'p.nama_tamu',
            'p.tanggal_cekin',
            'p.tanggal_cekout',
            'p.telpon',
            'p.harga',
            'p.status',
            'p.created_at',
            // 'pe.id',
            'pe.vila'
            ])
            ->where('p.id', $id)
            ->first();
            $pdf = PDF::loadView('booking.kuwintansi_1', get_object_vars($this));
        return $pdf->download('kuwintansi.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user()->name;
        $this->product = DB::table('tbl_product')
        ->where('created_by','=',$user)
        ->orderBy('vila', 'asc')->get();
        $this->data = BookingModel::find($id);
        return view('booking.edit', get_object_vars($this));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $booking = BookingModel::find($request->id);
        $booking->vila = "Vila ku ku";
        $booking->idProduct = $request->idProduct;
        $booking->nama_tamu = $request->nama_tamu;
        $booking->status = $request->status;
        $booking->harga = $request->harga;
        $booking->tanggal_cekin = $request->tanggal_cekin;
        $booking->tanggal_cekout = $request->tanggal_cekout;
        $booking->save();
        return redirect('booking')->withSuccess('Data Booking berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        BookingModel::destroy($request->id);
        return redirect('booking')->withSuccess('Data Berhasil Di hapus');
    }
}
