<?php

namespace App\Http\Controllers;
use App\Models\BookingModel;
use App\Models\CalenderModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
use LaravelFullCalendar\Facades\Calendar;
class CalenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
     $events = array();
     $user = Auth::user()->status;
    //  if($user=='marketing'){
    //     $bookings = BookingModel::all();     
    //  }else{
    //     $bookings = DB::table('tbl_booking')->where('created_by','=',Auth::user()->name)
    //     ->get();
    //  }
    if($user=='marketing'){
        $bookings = DB::table('tbl_booking as p')
        ->join('tbl_product as pe', 'p.idProduct','=','pe.id')   
        
        ->select([
            'p.id',
            'p.nama_tamu',
            'p.tanggal_cekin',
            'p.tanggal_cekout',
            'p.status',
            'p.created_at',
            'pe.vila'])
            ->get();
    }else{
        $bookings = DB::table('tbl_booking as p')
        ->join('tbl_product as pe', 'p.idProduct','=','pe.id')   
        
        ->select([
            'p.id',
            'p.nama_tamu',
            'p.tanggal_cekin',
            'p.tanggal_cekout',
            'p.status',
            'p.created_at',
            'pe.vila'])
            ->where('p.created_by','=',Auth::user()->name)
            ->get();
    }
    // $bookings = DB::table('tbl_booking as p')
    // ->join('tbl_product as pe', 'p.idProduct','=','pe.id')   
    
    // ->select([
    //     'p.id',
    //     'p.nama_tamu',
    //     'p.tanggal_cekin',
    //     'p.tanggal_cekout',
    //     'p.status',
    //     'p.created_at',
    //     'pe.vila'])
    //     ->where('p.created_by','=',Auth::user()->name)
    //     ->get();

        foreach($bookings as $booking){
            $color = null;
            if($booking->status == 'Cancel'){
                $color = '#F42107';
            }
            if($booking->status == 'Lunas' || $booking->status == 'DP'){
                $color = '#1EE707';
                // return $color;
            }
            $events[] = [
                'title' => $booking->vila,
                'start' => $booking->tanggal_cekin,
                'end'   => $booking->tanggal_cekout,
                'color' => $color   
            ];
        }
        // return $events;
        return view('calender.index', ['events' =>$events]);
    }

    public function indexPart2(Request $request)
    {
  // test fungcion
     $events = CalenderModel::all();
     $event = [];
        foreach($events as $row){

            $event[] = \Calendar::event(
                $row->title,
                false,
                new \Date($row->start_date),
                new \Date($row->end_date),
                $row->id

            );
        }
        $calendar = \Calendar::addEvents($event);
        return view('calender.index', compact('events','calendar'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
