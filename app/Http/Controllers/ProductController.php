<?php

namespace App\Http\Controllers;
use App\Models\DataProdakModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Yajra\DataTables\DataTables;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('prodak.index', get_object_vars($this)); 
    }
    public function getListProdak(Request $request){
        $user = Auth::user()->name;
        if($user =='admin'){
            $product = DB::table('tbl_product')
            ->select(['id','vila','kamar','kapasitas','created_at'])
            ->get();
        }else{
            $product = DB::table('tbl_product')
            ->select(['id','vila','kamar','kapasitas','created_at'])
            ->where('created_by','=',$user)
            ->get();
        }
        
        $datatables = Datatables::of($product)->addColumn('action', function ($product) {
            return '<a href="prodak/'. $product->id .'" "  class="btn btn-xs btn-primary"><i class="icon ion-edit" aria-hidden="true"></i></i></a>  
            <a href="#" data-id="'.$product->id.'" data-nama="'.$product->vila.'"class="btn btn-xs btn-danger destroy"><i class="icon ion-trash-a" ></i></a>';
    
        })

        ->escapeColumns([]);

        return $datatables->make(true);
    }
    public function form()
    {
        return view('prodak.create');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $product = new DataProdakModel;
        $product->vila = $request->vila;
        $product->kamar = $request->kamar;
        $product->kapasitas = $request->kapasitas;
        $product->created_by = Auth::user()->name;
        $product->save();
        return redirect('prodak')->withSuccess('Data Berhasil Disimpan ');;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->data = DataProdakModel::find($id);
        return view('prodak.edit', get_object_vars($this));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $product = DataProdakModel::find($request->id);
        $product->vila = $request->vila;
        $product->kamar = $request->kamar;
        $product->kapasitas = $request->kapasitas;
        $product->updated_by =Auth::user()->name;
        $product->save();
        return redirect('prodak')->withSuccess('Data berhasil di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        DataProdakModel::destroy($request->id);
        return redirect('prodak')->withSuccess('Data Berhasil Di hapus');
    }
}
